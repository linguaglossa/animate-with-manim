# animate-with-manim



## What is manim

Manim is a tool for **conveying complex messages** that can be difficult to understand without **spatial AND temporal** visualization.

## What manim is not

- It is not a visualization tool
- It is not an interactive event handler
- It is not a movie maker


## Install

The easiest way is via ```pip```

```
pip install manim
```

The installation is pretty straightforward. However, some libraries may be missing. In my case : 

```
sudo apt-get install ffmpeg libavcodec-extra libpango1.0-dev
```

Other methods include Docker and building from source. 

Manim is available for all major platforms.


## Integrate with your tools

